{*******************************************************}
{                                                       }
{           diggets.com Gorilla3D Framework             }
{                                                       }
{          Copyright(c) 2017-2018 Eric Berger           }
{              All rights reserved                      }
{                                                       }
{   Copyright and license exceptions noted in source    }
{                                                       }
{*******************************************************}

{*******************************************************}
{                                                       }
{  SHADER CODE:                                         }
{  Based on Shanes' Fiery Spikeball                     }
{  https://www.shadertoy.com/view/4lBXzy                }
{  Relief come from Siggraph workshop by Beautypi/2015  }
{  https://www.shadertoy.com/view/MtsSRf                }
{  License Creative Commons                             }
{  Attribution-NonCommercial-ShareAlike 3.0             }
{                                                       }
{*******************************************************}

unit Gorilla.Material.Sun;

interface

uses
  System.SysUtils,
  System.Classes,
  System.Messaging,
  System.Math.Vectors,
  System.Types,
  System.UITypes,
  FMX.Types,
  FMX.Types3D,
  FMX.Graphics,
  FMX.Materials,
  FMX.MaterialSources,
  Gorilla.Controller,
  Gorilla.Context.GLES;

type
  {$M+}
  TGorillaSunMaterialSource = class;

  /// <summary>
  /// 
  /// </summary>
  TGorillaSunMaterial = class(TCustomMaterial)
  private
    FSource       : TGorillaSunMaterialSource;
    FTimerService : IFMXTimerService;
    FStartTime    : Double;
    FRenderTime   : Double;
    FLastTime     : Double;

  protected

    procedure DoApply(const Context: TContext3D); override;
    procedure DoInitialize(); override;

  public
    constructor Create(const ASource : TGorillaSunMaterialSource); reintroduce; virtual;
    destructor Destroy(); override;

  published
  end;

  /// <summary>
  /// 
  /// </summary>
  /// <remarks>
  /// Based on Shanes' Fiery Spikeball
  /// https://www.shadertoy.com/view/4lBXzy
  /// Relief come from Siggraph workshop by Beautypi/2015
  /// https://www.shadertoy.com/view/MtsSRf
  /// License Creative Commons
  /// Attribution-NonCommercial-ShareAlike 3.0
  /// </remarks>
  TGorillaSunMaterialSource = class(TMaterialSource)
  protected
    function CreateMaterial() : TMaterial; override;

  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy(); override;

  published
  end;

implementation

uses
  System.Math,
  System.IOUtils,
  FMX.Platform,
  FMX.Objects3D,
  FMX.Controls3D;

  ResourceString GORILLA_SUN_VS =
    'uniform mat4 _ModelViewProjMatrix;' +

    'attribute vec3 a_Position;' +
    'attribute vec3 a_Normal;' +
    'attribute vec4 a_Color;' +
    'attribute vec2 a_TexCoord0;' +
    'attribute vec3 a_Binormal;' +
    'attribute vec3 a_Tangent;' +

    'varying vec4 v_VertexPosition;' +
    'varying vec4 v_VertexColor;' +
    'varying vec2 v_TextureCoord;' +

    'void main( void ){' +
    '   v_VertexColor = a_Color;' +
    '   v_TextureCoord = a_TexCoord0;' +
    '   v_VertexPosition = _ModelViewProjMatrix * vec4(a_Position, 1.0);' +

    '   gl_Position = v_VertexPosition;' +

    '}';

  ResourceString GORILLA_SUN_FS_1 =
    'uniform vec2 _Resolution;'#10#13 +
    'uniform float _Time;'#10#13 +
    'uniform vec4 _EyeDir;'#10#13 +

    'varying vec4 v_VertexPosition;'#10#13 +
    'varying vec4 v_VertexColor;'#10#13 +
    'varying vec2 v_TextureCoord;'#10#13 +

    'const vec4 _Mouse = vec4(320.0, 240.0, 0.0, 0.0);'#10#13 +

    // animated noise
    'vec4 NC0 = vec4(0.0, 157.0, 113.0, 270.0);'#10#13 +
    'vec4 NC1 = vec4(1.0, 158.0, 114.0, 271.0);'#10#13 +
    'vec4 WS = vec4(0.25, 0.25, 0.25, 0.25);'#10#13 +

    // mix noise for alive animation, full source
    'vec4 hash4(vec4 n){'#10#13 +
    '  return fract(sin(n)*1399763.5453123);'#10#13 +
    '}'#10#13 +

    'vec3 hash3(vec3 n){'#10#13 +
    '  return fract(sin(n)*1399763.5453123);'#10#13 +
    '}' +

    'vec3 hpos(vec3 n){'#10#13 +
    '  return hash3(vec3(dot(n, vec3(157.0, 113.0, 271.0)), dot(n, vec3(271.0, 157.0, 113.0)), dot(n, vec3(113.0, 271.0, 157.0))));'#10#13 +
    '}'#10#13 +

    'float noise4q(vec4 x){'#10#13 +
    '  vec4 x_xxxx = vec4(x.x, x.x, x.x, x.x);' +
    '  vec4 x_yyyy = vec4(x.y, x.y, x.y, x.y);' +
    '  vec4 x_zzzz = vec4(x.z, x.z, x.z, x.z);' +
    '  vec4 x_wwww = vec4(x.w, x.w, x.w, x.w);' +

    '  vec4 n3 = vec4(0, 0.25, 0.5, 0.75);'#10#13 +
    '  vec4 p2 = floor(x_xxxx + n3);'#10#13 +

    '  vec4 b = floor(x_xxxx + n3) + floor(x_yyyy + n3) * 157.0 + floor(x_zzzz + n3) * 113.0;'#10#13 +
    '  vec4 p1 = b + fract(p2 * 0.00390625) * vec4(164352.0, -164352.0, 163840.0, -163840.0);'#10#13 +
    '  p2 = b + fract((p2 + 1.0) * 0.00390625) * vec4(164352.0, -164352.0, 163840.0, -163840.0);'#10#13 +
    '  vec4 f1 = fract(x_xxxx + n3);'#10#13 +
    '  vec4 f2 = fract(x_yyyy + n3);'#10#13 +
    '  f1 = f1 * f1 * (3.0 - 2.0 * f1);'#10#13 +
    '  f2 = f2 * f2 * (3.0 - 2.0 * f2);'#10#13 +
    '  vec4 n1 = vec4(0, 1.0, 157.0, 158.0);'#10#13 +
    '  vec4 n2 = vec4(113.0, 114.0, 270.0, 271.0);'#10#13 +

    '  vec4 n1_yyyy = vec4(n1.y, n1.y, n1.y, n1.y);' +
    '  vec4 n1_zzzz = vec4(n1.z, n1.z, n1.z, n1.z);' +
    '  vec4 n1_wwww = vec4(n1.w, n1.w, n1.w, n1.w);' +

    '  vec4 n2_xxxx = vec4(n2.x, n2.x, n2.x, n2.x);' +
    '  vec4 n2_yyyy = vec4(n2.y, n2.y, n2.y, n2.y);' +
    '  vec4 n2_zzzz = vec4(n2.z, n2.z, n2.z, n2.z);' +
    '  vec4 n2_wwww = vec4(n2.w, n2.w, n2.w, n2.w);' +

    '  vec4 vs1 = mix(hash4(p1), hash4(n1_yyyy + p1), f1);'#10#13 +
    '  vec4 vs2 = mix(hash4(n1_zzzz + p1), hash4(n1_wwww + p1), f1);'#10#13 +

    '  vec4 vs3 = mix(hash4(p2), hash4(n1_yyyy + p2), f1);'#10#13 +
    '  vec4 vs4 = mix(hash4(n1_zzzz + p2), hash4(n1_wwww + p2), f1);'#10#13 +

    '  vs1 = mix(vs1, vs2, f2);'#10#13 +
    '  vs3 = mix(vs3, vs4, f2);'#10#13 +
    '  vs2 = mix(hash4(n2_xxxx + p1), hash4(n2_yyyy + p1), f1);'#10#13 +
    '  vs4 = mix(hash4(n2_zzzz + p1), hash4(n2_wwww + p1), f1);'#10#13 +
    '  vs2 = mix(vs2, vs4, f2);'#10#13 +
    '  vs4 = mix(hash4(n2_xxxx + p2), hash4(n2_yyyy + p2), f1);'#10#13 +
    '  vec4 vs5 = mix(hash4(n2_zzzz + p2), hash4(n2_wwww + p2), f1);'#10#13 +

    '  vs4 = mix(vs4, vs5, f2);'#10#13 +
    '  f1 = fract(x_zzzz + n3);'#10#13 +
    '  f2 = fract(x_wwww + n3);'#10#13 +
    '  f1 = f1 * f1 * (3.0 - 2.0 * f1);'#10#13 +
    '  f2 = f2 * f2 * (3.0 - 2.0 * f2);'#10#13 +
    '  vs1 = mix(vs1, vs2, f1);'#10#13 +
    '  vs3 = mix(vs3, vs4, f1);'#10#13 +
    '  vs1 = mix(vs1, vs3, f2);'#10#13 +
    '  float r = dot(vs1, vec4(0.25));'#10#13 +
    '  return r * r * (3.0 - 2.0 * r);'#10#13 +
    '}' +

    // body of a star
    'float noiseSpere(vec3 ray,vec3 pos,float r,mat3 mr,float zoom,vec3 subnoise,float anim){'#10#13 +
    '  return 0.0;' +
    (*
    '  float b = dot(ray, pos);'#10#13 +
    '  float c = dot(pos, pos) - b*b;'#10#13 +

    '  vec3 r1 = vec3(0.0);'#10#13 +

    '  float s = 0.0;'#10#13 +
    '  float d = 0.03125;'#10#13 +
    '  float d2 = zoom / (d * d);'#10#13 +
    '  float ar = 5.0;'#10#13 +

    '  for (int i = 0; i < 3; i++){'#10#13 +
    '    float rq = r * r;'#10#13 +
    '    if(c < rq){'#10#13 +
    '      float l1 = sqrt(rq - c);'#10#13 +
    '      r1 = ray *(b - l1) - pos;'#10#13 +
    '      r1 = r1 * mr;'#10#13 +
    '      s += abs(noise4q(vec4(r1 * d2 + subnoise * ar, anim * ar)) * d);'#10#13 +
    '    }'#10#13 +
    '    ar -= 2.0;'#10#13 +
    '    d *= 4.0;'#10#13 +
    '    d2 *= 0.0625;'#10#13 +
    '    r = r - r * 0.02;'#10#13 +
    '  }'#10#13 +
    '  return s;'#10#13 +
    *)
    '}'#10#13;

  ResourceString GORILLA_SUN_FS_2 =

    // glow ring
    'float ring(vec3 ray, vec3 pos, float r, float size){'#10#13 +
    '  float b = dot(ray, pos);'#10#13 +
    '  float c = dot(pos, pos) - b * b;'#10#13 +

    '  float s = max(0.0, (1.0 - size * abs(r - sqrt(c))));'#10#13 +

    '  return s;'#10#13 +
    '}' +

    // rays of a star
    'float ringRayNoise(vec3 ray, vec3 pos, float r, float size, mat3 mr, float anim){'#10#13 +
    '  float b = dot(ray, pos);'#10#13 +
    '  vec3 pr = ray * b - pos;'#10#13 +

    '  float c = length(pr);'#10#13 +
    '  pr *= mr;'#10#13 +
    '  pr = normalize(pr);'#10#13 +

    '  float s = max(0.0, (1.0 - size * abs(r - c)));'#10#13 +

    '  float nd = noise4q(vec4(pr * 1.0, -anim + c)) * 2.0;'#10#13 +
    '  nd = pow(nd, 2.0);'#10#13 +
    '  float n = 0.4;'#10#13 +
    '  float ns = 1.0;'#10#13 +
    '  if (c > r){'#10#13 +
    '    n = noise4q(vec4(pr * 10.0, -anim + c));'#10#13 +
    '    ns = noise4q(vec4(pr * 50.0, -anim * 2.5 + c * 2.0)) * 2.0;'#10#13 +
    '  }'#10#13 +
    '  n = n * n * nd * ns;'#10#13 +

    '  return pow(s, 4.0) + s * s * n;'#10#13 +
    '}'#10#13 +

    'vec4 noiseSpace(vec3 ray, vec3 pos, float r, mat3 mr, float zoom, vec3 subnoise, float anim){'#10#13 +
    '  return vec4(0.0);' +
    (*
    '  float b = dot(ray, pos);'#10#13 +
    '  float c = dot(pos, pos) - b * b;'#10#13 +

    '  vec3 r1 = vec3(0.0);'#10#13 +

    '  float s = 0.0;'#10#13 +
    '  float d = 0.0625 * 1.5;'#10#13 +
    '  float d2 = zoom / d;'#10#13 +

    '  float rq = r * r;'#10#13 +
    '  float l1 = sqrt(abs(rq - c));'#10#13 +
    '  r1 = (ray * (b - l1) - pos) * mr;'#10#13 +

    '  r1 *= d2;'#10#13 +
    '  s += abs(noise4q(vec4(r1 + subnoise, anim)) * d);'#10#13 +
    '  s += abs(noise4q(vec4(r1 * 0.5 + subnoise, anim)) * d * 2.0);'#10#13 +
    '  s += abs(noise4q(vec4(r1 * 0.25 + subnoise, anim))* d * 4.0);'#10#13 +
    '  return vec4(s * 2.0, abs(noise4q(vec4(r1 * 0.1 + subnoise, anim))),abs(noise4q(vec4(r1 * 0.1 + subnoise * 6.0, anim))), abs(noise4q(vec4(r1 * 0.1 + subnoise * 13.0, anim))));'#10#13 +
    *)
    '}'#10#13 +

    'float sphereZero(vec3 ray, vec3 pos, float r){'#10#13 +
    '  return 0.0;' +
    (*
    '  float b = dot(ray, pos);'#10#13 +
    '  float c = dot(pos, pos) - b * b;'#10#13 +
    '  float s = 1.0;'#10#13 +
    '  if (c < r * r) s = 0.0;'#10#13 +
    '  return s;'#10#13 +
    *)
    '}'#10#13;

  ResourceString GORILLA_SUN_FS_3 =
    'void mainImage(out vec4 fragColor, in vec2 fragCoord){'#10#13 +
//    '  vec2 p = (-_Resolution.xy + 2.0 * fragCoord.xy) / _Resolution.y;'#10#13 +
    '  float time = _Time * 1.0;'#10#13 +

//    '  float mx = _Mouse.z > 0.0 ? _Mouse.x / _Resolution.x * 10.0 : time * 0.025;'#10#13 +
//    '  float my = _Mouse.z > 0.0 ? _Mouse.y / _Resolution.y * 4.0-2.0 : -0.6;'#10#13 +
    '  float mx = time * 0.025;' +
    '  float my = -0.6;' +
    '  vec2 rotate = vec2(mx, my);'#10#13 +

    '  vec2 sins = sin(rotate);'#10#13 +
    '  vec2 coss = cos(rotate);'#10#13 +
    '  mat3 mr = mat3(vec3(coss.x, 0.0, sins.x), vec3(0.0, 1.0, 0.0), vec3(-sins.x, 0.0, coss.x));'#10#13 +
    '  mr = mat3(vec3(1.0, 0.0, 0.0), vec3(0.0, coss.y, sins.y), vec3(0.0, -sins.y, coss.y)) * mr;'#10#13 +

    '  mat3 imr = mat3(vec3(coss.x, 0.0, -sins.x), vec3(0.0, 1.0, 0.0), vec3(sins.x, 0.0, coss.x));'#10#13 +
    '  imr = imr * mat3(vec3(1.0, 0.0, 0.0),vec3(0.0, coss.y, -sins.y), vec3(0.0, sins.y, coss.y));'#10#13 +

//    '  vec3 ray = normalize(vec3(p, 2.0));'#10#13 +
//    '  vec3 pos = vec3(0.0, 0.0, 3.0);'#10#13 +
    '  vec3 ray = _EyeDir.xyz;' +
    '  vec3 pos = v_VertexPosition.xyz;'#10#13 +

    '  float s1 = noiseSpere(ray, pos, 1.0, mr, 0.5, vec3(0.0), time);'#10#13 +
    '  s1 = pow(min(1.0, s1 * 2.4), 2.0);'#10#13 +
    '  float s2 = noiseSpere(ray, pos, 1.0, mr, 4.0, vec3(83.23, 34.34, 67.453), time);'#10#13 +
    '  s2 = min(1.0, s2 * 2.2);'#10#13 +
    '  fragColor = vec4( mix(vec3(1.0, 1.0, 0.0), vec3(1.0), pow(s1, 60.0)) * s1, 1.0 );'#10#13 +
    '  fragColor += vec4( mix(mix(vec3(1.0, 0.0, 0.0),vec3(1.0, 0.0, 1.0),pow(s2, 2.0)), vec3(1.0), pow(s2, 10.0)) * s2, 1.0 );'#10#13 +

    '  fragColor.xyz -= vec3(ring(ray, pos, 1.03, 11.0)) * 2.0;'#10#13 +
    '  fragColor = max( vec4(0.0), fragColor );'#10#13 +

    '  float s3 = ringRayNoise(ray, pos, 0.96, 1.0, mr, time);'#10#13 +
    '  fragColor.xyz += mix(vec3(1.0, 0.6, 0.1), vec3(1.0, 0.95, 1.0), pow(s3, 3.0)) * s3;'#10#13 +

    '  float zero = sphereZero(ray, pos, 0.9);'#10#13 +
    '  if (zero > 0.0) {'#10#13 +
    '    vec4 s4 = noiseSpace(ray, pos, 100.0, mr, 0.05, vec3(1.0, 2.0, 4.0), 0.0);'#10#13 +
    '    s4.x = pow(s4.x, 3.0);'#10#13 +
    '    fragColor.xyz += mix(mix(vec3(1.0, 0.0, 0.0), vec3(0.0, 0.0, 1.0), s4.y * 1.9), vec3(0.9, 1.0, 0.1), s4.w * 0.75) * s4.x * pow(s4.z * 2.5, 3.0) * 0.2 * zero;'#10#13 +
    '  }'#10#13 +

    '  fragColor = max(vec4(0.0), fragColor);'#10#13 +
    '  fragColor = min(vec4(1.0), fragColor);'#10#13 +
    '}'#10#13 +

    'void main(){'#10#13 +
    '  vec4 l_Color;'#10#13 +
    '  mainImage(l_Color, v_TextureCoord.xy);'#10#13 + //gl_FragCoord.xy);' +

    '  if((l_Color.r < 0.05) && (l_Color.g < 0.05) && (l_Color.b < 0.05)) {' +
    '    discard;' +
    '    return;' +
    '  }' +

    '  float l_Alpha = l_Color.a;' +
    '  if((l_Color.r < 0.1) && (l_Color.g < 0.1) && (l_Color.b < 0.1)) {' +
    '    float l_Alpha = 0.25;' +
    '  }' +

    '  gl_FragColor = vec4(l_Color.b, l_Color.g, l_Color.r, l_Alpha);'#10#13 +
    '}'#10#13;

{ TGorillaSunMaterial }

constructor TGorillaSunMaterial.Create(const ASource : TGorillaSunMaterialSource);
begin
  inherited Create();

  FSource := ASource;

  if FTimerService = nil then
    if not TPlatformServices.Current.SupportsPlatformService(IFMXTimerService, FTimerService) then
      raise EUnsupportedPlatformService.Create('IFMXTimerService');

  FLastTime   := FTimerService.GetTick();
  FRenderTime := FLastTime;
  FStartTime  := FLastTime;
end;

destructor TGorillaSunMaterial.Destroy();
begin
  inherited;
end;

procedure TGorillaSunMaterial.DoInitialize();
var LOGLBytes : TArray<Byte>;
begin
  ///
  /// VertexShader
  ///
  LOGLBytes := TEncoding.ASCII.GetBytes(GORILLA_SUN_VS);
  FVertexShader := TShaderManager.RegisterShaderFromData('sun.fvs', TContextShaderKind.VertexShader, '', [
    TContextShaderSource.Create(TContextShaderArch.GLSL,
      LOGLBytes,
      [
      TContextShaderVariable.Create('ModelViewProjMatrix', TContextShaderVariableKind.Matrix, 0, 4),
      TContextShaderVariable.Create('ModelViewMatrix', TContextShaderVariableKind.Matrix, 1, 4)
      ]
    )
  ]);


  ///
  /// PixelShader / FragmentShader
  ///
  LOGLBytes := TEncoding.ASCII.GetBytes(GORILLA_SUN_FS_1 + GORILLA_SUN_FS_2 + GORILLA_SUN_FS_3);
  FPixelShader := TShaderManager.RegisterShaderFromData('sun.fps', TContextShaderKind.PixelShader, '', [
    TContextShaderSource.Create(TContextShaderArch.GLSL,
      LOGLBytes,
      [
      TContextShaderVariable.Create('Time', TContextShaderVariableKind.Float, 0, 1),
      TContextShaderVariable.Create('Resolution', TContextShaderVariableKind.Float2, 1, 1),
      TContextShaderVariable.Create('EyeDir', TContextShaderVariableKind.Vector, 2, 1)
      ]
    )
  ]);
end;


procedure TGorillaSunMaterial.DoApply(const Context: TContext3D);
var
  LCtx        : TCustomContextOpenGL;
  LTime       : Single;
  LResolution : TPointF;
  LEyeDir     : TVector3D;
  LOwner      : TControl3D;
begin
  LCtx := TCustomContextOpenGL(Context);

  LResolution := PointF(Context.Width, Context.Height);

  FLastTime   := FRenderTime;
  FRenderTime := FTimerService.GetTick();

  // set shaders
  LCtx.SetShaders(FVertexShader, FPixelShader);

  // textures
  TCustomContextOpenGL(Context).SetShaderVariable('ModelViewProjMatrix', LCtx.CurrentModelViewProjectionMatrix, true);
  TCustomContextOpenGL(Context).SetShaderVariable('ModelViewMatrix', LCtx.CurrentMatrix, true);

  LTime := (FRenderTime - FStartTime) * 0.5;

  Context.SetShaderVariable('Time', [Vector3D(LTime, 0, 0, 0)]);
  Context.SetShaderVariable('Resolution', [Vector3D(LResolution.X, LResolution.Y, 0, 0)]);

  LOwner  := Self.FSource.Parent as TControl3D;
  LEyeDir := LOwner.AbsoluteDirection;

  Context.SetShaderVariable('EyeDir', [LEyeDir]);
end;











{ TGorillaSunMaterialSource }

constructor TGorillaSunMaterialSource.Create(AOwner: TComponent);
begin
  inherited;
end;

destructor TGorillaSunMaterialSource.Destroy();
begin
  inherited;
end;

function TGorillaSunMaterialSource.CreateMaterial() : TMaterial;
begin
  Result := TGorillaSunMaterial.Create(Self);
end;

initialization
  RegisterFmxClasses([TGorillaSunMaterialSource]);

end.