## Gorilla3D (Developer-Edition) Addon Framework for Delphi Firemonkey 3D

---

## Features
By default Delphi Firemonkey 3D library is very limited. By Gorilla3D you can extend the 3D functionality.

We provide a lot of new features and components:

* Skin-, Skeleton- und Vertexanimations for 3D Meshes (Key-Animations)
* Integrated and fast Q3 Physics Engine (with collision detection)
* Materials: textures, normal-mapping, bump-mapping, vertex-color, phong-, blinn-, lambert-materials, water and reflection, layered materials, splatmaps, terrain-material
* Order-Independent Rendering for better transparency results
* Loading complex multi-mesh models with UV-textures
* Logical model management (independent from FireMonkey tree), to manage a large amount of meshes and to instanciate separatly
* Flexible particle-system with influencer-classes (up to 100K particle at runtime)
* Shadow- and Variance-Shadow-Mapping (experimental)
* Multipass-Rendering with easy controlling
* Bokeh/Depth-Of-Field new feature
* SkyBox-support with CubeMap
* Texture3D support
* Fog rendering (linear, exp, exp2)
* Water Surface rendering
* Terrain rendering: from height-maps and procedural algorithms (Diamond-Square, Perlin-Noise, ...)
* CVLOD Terrain component for level of detail detail runtime management
* New Point3D-, Quaternion-, Transformationsmatrix- und VertexKey-Animationen
* FMOD AudioManager for professional audio playback new feature
* AssetsManager with packaging system new feature
* Flexible DialogueSystem: load, save and edit at runtime
* Flexible InventorySystem: load, save and edit at runtime
* Flexible SkillSystem: load, save and edit at runtime
* Fast 3D model loading
* Compatible with existing FireMonkey components
* Platform-independent: WIN32, WIN64, ANDROID, ANDROID64 [currently not supported: iOS, MACOS, LINUX]
* Formate: G3D new feature, X3D, X3DZ, X3DVZ, OBJ, STL, DAE, FBX

---
## Developer Edition Download

Because we are also a small company, we know how hard and expensive it is to develop an application,
without knowing if it's ever going to be published.
Therefor we provide the free downloadable developer edition here.

During your development process you are allowed to build and test your program with that package.
If you are ready for release, you need to buy the commercial license.
Contact us at: gorilla3d@diggets.com

The developer edition is limited to maximum of 5 developers working on a project.
It contains the full component set, but is compiled with watermarks.
You are not allowed to publish or sell your application build with the developer edition.

---

## Install
* https://docs.gorilla3d.de/doku.php?id=installation

---

## Links
* Visit us at: https://gorilla3d.de/
* Report bugs at: https://dev.gorilla3d.de/
* Online documentation: https://docs.gorilla3d.de/

---
## Social Media

Follow us at:
1. Instagram: https://www.instagram.com/_diggets_/

2. Facebook: https://fb.me/diggets

3. Youtube: https://www.youtube.com/channel/UCur5cOVgIKJHDncK5IiFoSw

4. Telegram-Stream: https://t.me/gorilla3d

5. Telegram-Chat: https://t.me/Gorilla3dGroup