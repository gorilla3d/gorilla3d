unit Unit1;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,
  Gorilla.UI.AudioPlayer, Gorilla.Audio.FMOD,
  FMX.Controls.Presentation, FMX.StdCtrls;

type
  TForm1 = class(TForm)
    GorillaFMODAudioManager1: TGorillaFMODAudioManager;
    Button1: TButton;
    Label1: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure Button1Click(Sender: TObject);
  private
    FCurrent : Integer;

    procedure ShowSoundName();
  public
    { Public-Deklarationen }
  end;

var
  Form1: TForm1;

implementation

{$R *.fmx}

uses
  System.IOUtils;

procedure TForm1.Button1Click(Sender: TObject);
begin
  inc(FCurrent);
  if (FCurrent >= 3) then FCurrent := 0;

  ShowSoundName();
  GorillaFMODAudioManager1.PlaySound(FCurrent);
end;

procedure TForm1.FormCreate(Sender: TObject);
var LPath : String;
begin
{$IFDEF MSWINDOWS}
  LPath := '';
{$ENDIF}
{$IFDEF ANDROID}
  LPath := IncludeTrailingPathDelimiter(TPath.GetHomePath());
{$ENDIF}

  FCurrent := 0;
  GorillaFMODAudioManager1.LoadSoundFromFile(LPath + 'drumloop-2.ogg');
  GorillaFMODAudioManager1.LoadSoundFromFile(LPath + 'explosion.mp3');
  GorillaFMODAudioManager1.LoadSoundFromFile(LPath + 'jaguar.wav');

  ShowSoundName();
  GorillaFMODAudioManager1.PlaySound(FCurrent);
end;

procedure TForm1.ShowSoundName();
begin
  case FCurrent of
    0 : Label1.Text := 'drumloop-2.ogg';
    1 : Label1.Text := 'explosion.mp3';
    2 : Label1.Text := 'jaguar.wav';
    else Label1.Text := 'unknown';
  end;
end;

end.
