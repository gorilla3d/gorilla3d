unit Unit1;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,
  Gorilla.Viewport, System.Math.Vectors, FMX.Types3D, Gorilla.Control,
  Gorilla.Mesh, Gorilla.Cube, FMX.Controls3D, FMX.Objects3D, Gorilla.Controller,
  Gorilla.Controller.Input, Gorilla.Controller.Input.Consts;

type
  TForm1 = class(TForm)
    GorillaViewport1: TGorillaViewport;
    Dummy1: TDummy;
    Camera1: TCamera;
    Light1: TLight;
    GorillaCube1: TGorillaCube;
    GorillaInputController1: TGorillaInputController;
    Timer1: TTimer;
    procedure FormCreate(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
  private
    procedure DoOnHotKeyA(const AItem : TGorillaHotKeyItem; const ACurrentInput : TGorillaHotKeyRaw);
    procedure DoOnHotKeyB(const AItem : TGorillaHotKeyItem; const ACurrentInput : TGorillaHotKeyRaw);
    procedure DoOnHotKeyX(const AItem : TGorillaHotKeyItem; const ACurrentInput : TGorillaHotKeyRaw);
    procedure DoOnHotKeyY(const AItem : TGorillaHotKeyItem; const ACurrentInput : TGorillaHotKeyRaw);
    procedure DoOnSequence(const ASequence : TGorillaInputSequenceItem);
  public
    { Public-Deklarationen }
  end;

var
  Form1: TForm1;

implementation

{$R *.fmx}

procedure TForm1.FormCreate(Sender: TObject);
var A, B, X, Y : TGorillaHotKeyItem;
    LSequence : TGorillaInputSequenceItem;
begin
  GorillaInputController1.Enabled := true;

  A := GorillaInputController1.AddHotKey('GAMEPAD_A');
  A.AddInput(TGorillaInputDeviceType.GamePad, GORILLA_INPUT_GAMEPAD_BUTTON_A);
  A.OnTriggered := DoOnHotKeyA;
  A.LockTime := 250;

  B := GorillaInputController1.AddHotKey('GAMEPAD_B');
  B.AddInput(TGorillaInputDeviceType.GamePad, GORILLA_INPUT_GAMEPAD_BUTTON_B);
  B.OnTriggered := DoOnHotKeyB;
  B.LockTime := 250;

  X := GorillaInputController1.AddHotKey('GAMEPAD_X');
  X.AddInput(TGorillaInputDeviceType.GamePad, GORILLA_INPUT_GAMEPAD_BUTTON_X);
  X.OnTriggered := DoOnHotKeyX;
  X.LockTime := 250;

  Y := GorillaInputController1.AddHotKey('GAMEPAD_Y');
  Y.AddInput(TGorillaInputDeviceType.GamePad, GORILLA_INPUT_GAMEPAD_BUTTON_Y);
  Y.OnTriggered := DoOnHotKeyY;
  Y.LockTime := 250;

  LSequence := GorillaInputController1.AddSequence('SEQUENCE', [A, B, X, Y]);
  LSequence.Sensitivity := 500;
  LSequence.OnTriggered := DoOnSequence;

  Timer1.Enabled := true;
end;

procedure TForm1.Timer1Timer(Sender: TObject);
var LMsg : TGorillaInputMessage;
    LX, LY : Single;
begin
  // get the left thumbstick position to rotate cube
  if GorillaInputController1.GetPersistentMessage(TGorillaInputDeviceType.GamePad,
    GORILLA_INPUT_GAMEPAD_THUMBSTICK_POS_LEFT, LMsg) then
  begin
    LX := High(Word) / Hi(LMsg.WParam);
    LY := High(Word) / Lo(LMsg.WParam);

    GorillaCube1.BeginUpdate();
    try
      GorillaCube1.RotationAngle.Point := GorillaCube1.RotationAngle.Point + Point3D(LX, 0, LY);
    finally
      GorillaCube1.EndUpdate();
    end;
  end;
end;

procedure TForm1.DoOnHotKeyA(const AItem : TGorillaHotKeyItem; const ACurrentInput : TGorillaHotKeyRaw);
begin
  GorillaCube1.AnimateFloat('RotationAngle.Y', 180, 0.5);
end;

procedure TForm1.DoOnHotKeyB(const AItem : TGorillaHotKeyItem; const ACurrentInput : TGorillaHotKeyRaw);
begin
  GorillaCube1.AnimateFloat('RotationAngle.Y', -180, 0.5);
end;

procedure TForm1.DoOnHotKeyX(const AItem : TGorillaHotKeyItem; const ACurrentInput : TGorillaHotKeyRaw);
begin
  GorillaCube1.AnimateFloat('RotationAngle.Z', 180, 0.5);
end;

procedure TForm1.DoOnHotKeyY(const AItem : TGorillaHotKeyItem; const ACurrentInput : TGorillaHotKeyRaw);
begin
  GorillaCube1.AnimateFloat('RotationAngle.Z', -180, 0.5);
end;

procedure TForm1.DoOnSequence(const ASequence : TGorillaInputSequenceItem);
begin
  GorillaCube1.AnimateFloat('Scale.X', 2, 0.5, TAnimationType.InOut);
  GorillaCube1.AnimateFloat('Scale.Y', 2, 0.5, TAnimationType.InOut);
  GorillaCube1.AnimateFloat('Scale.Z', 2, 0.5, TAnimationType.InOut);
end;

end.
