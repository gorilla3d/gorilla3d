unit Unit1;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,
  Gorilla.Viewport, System.Math.Vectors, FMX.Types3D, FMX.Controls3D,
  FMX.Objects3D, Gorilla.Control, Gorilla.Mesh, Gorilla.Sphere,
  Gorilla.Material.BumpMap, Gorilla.Material.NormalMap, Gorilla.Material.Blinn,
  Gorilla.Material.Phong, FMX.MaterialSources, Gorilla.Material.Default,
  Gorilla.Material.Lambert, Gorilla.Material.Layered, FMX.ListBox,
  Gorilla.Material.SplatMap;

type
  TForm1 = class(TForm)
    GorillaViewport1: TGorillaViewport;
    GorillaSphere1: TGorillaSphere;
    Dummy1: TDummy;
    Camera1: TCamera;
    Light1: TLight;
    GorillaLambertMaterialSource1: TGorillaLambertMaterialSource;
    GorillaPhongMaterialSource1: TGorillaPhongMaterialSource;
    GorillaBlinnMaterialSource1: TGorillaBlinnMaterialSource;
    GorillaNormalMapMaterialSource1: TGorillaNormalMapMaterialSource;
    GorillaBumpMapMaterialSource1: TGorillaBumpMapMaterialSource;
    GorillaSplatMapMaterialSource1: TGorillaSplatMapMaterialSource;
    GorillaLayeredMaterialSource1: TGorillaLayeredMaterialSource;
    LightMaterialSource1: TLightMaterialSource;
    LightMaterialSource2: TLightMaterialSource;
    GorillaBumpMapMaterialSource2: TGorillaBumpMapMaterialSource;
    ComboBox1: TComboBox;
    Timer1: TTimer;

    procedure FormCreate(Sender: TObject);
    procedure ComboBox1Change(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
  private
    { Private-Deklarationen }
  public
    { Public-Deklarationen }
  end;

var
  Form1: TForm1;

implementation

{$R *.fmx}

uses
  System.IOUtils;

procedure TForm1.ComboBox1Change(Sender: TObject);
begin
  case ComboBox1.ItemIndex of
    0 : GorillaSphere1.MaterialSource := GorillaLambertMaterialSource1;
    1 : GorillaSphere1.MaterialSource := GorillaPhongMaterialSource1;
    2 : GorillaSphere1.MaterialSource := GorillaBlinnMaterialSource1;
    3 : GorillaSphere1.MaterialSource := GorillaNormalMapMaterialSource1;
    4 : GorillaSphere1.MaterialSource := GorillaBumpMapMaterialSource1;
    5 : GorillaSphere1.MaterialSource := GorillaSplatMapMaterialSource1;
    6 : GorillaSphere1.MaterialSource := GorillaLayeredMaterialSource1;
    7 : GorillaSphere1.MaterialSource := GorillaBumpMapMaterialSource2;

    else ShowMessage('no valid material index');
  end;

  GorillaViewport1.Invalidate();
end;

procedure TForm1.FormCreate(Sender: TObject);
var LTexPath : String;
begin
  LTexPath := IncludeTrailingPathDelimiter('textures');
{$IFDEF ANDROID}
  LTexPath := IncludeTrailingPathDelimiter(TPath.GetHomePath()) + LTexPath;
{$ENDIF}

  // load lambert material
  GorillaLambertMaterialSource1.Texture.LoadFromFile(LTexPath + 'gift-med.jpg');

  // load phong material
  GorillaPhongMaterialSource1.Texture.LoadFromFile(LTexPath + 'gift-med.jpg');

  // load blinn phong material
  GorillaBlinnMaterialSource1.Texture.LoadFromFile(LTexPath + 'gift-med.jpg');

  // load normal map material
  GorillaNormalMapMaterialSource1.Texture.LoadFromFile(LTexPath + 'crate2_diffuse.png');
  GorillaNormalMapMaterialSource1.NormalTexture.LoadFromFile(LTexPath + 'crate2_normal.png');

  // load bump map material
  GorillaBumpMapMaterialSource1.Texture.LoadFromFile(LTexPath + 'crate1_diffuse.png');
  GorillaBumpMapMaterialSource1.NormalTexture.LoadFromFile(LTexPath + 'crate1_normal.png');
  GorillaBumpMapMaterialSource1.SpecularTexture.LoadFromFile(LTexPath + 'crate1_bump.png');

  // load splatmap material
  GorillaSplatMapMaterialSource1.Texture.LoadFromFile(LTexPath + 'multi-splatmap.png');
  GorillaSplatMapMaterialSource1.SplatTexture0.LoadFromFile(LTexPath + 'floor-wood.jpg');
  GorillaSplatMapMaterialSource1.SplatTexture1.LoadFromFile(LTexPath + 'ground2.jpg');
  GorillaSplatMapMaterialSource1.SplatTexture2.LoadFromFile(LTexPath + 'layingrock-c.jpg');
  GorillaSplatMapMaterialSource1.SplatTexture3.LoadFromFile(LTexPath + 'grass.jpg');
  GorillaSplatMapMaterialSource1.UseTexturing := true;
  GorillaSplatMapMaterialSource1.UseLighting := true;

  // load layered material
  // LightMaterialSource1 and LightMaterialSource2 are already linked in design-mode
  // by Materials property of layered material source.
  // here we only load the textures for each layer
  LightMaterialSource1.Texture.LoadFromFile(LTexPath + 'multi-splatmap.png');
  LightMaterialSource2.Texture.LoadFromFile(LTexPath + 'multi-splatmap2.png');

  // load bumpmap material with displacement map
  GorillaBumpMapMaterialSource2.Texture.LoadFromFile(LTexPath + 'harshbricks-albedo.png');
  GorillaBumpMapMaterialSource2.NormalTexture.LoadFromFile(LTexPath + 'harshbricks-normal.png');
  GorillaBumpMapMaterialSource2.SpecularTexture.LoadFromFile(LTexPath + 'harshbricks-ao2.png');
  GorillaBumpMapMaterialSource2.DisplacementMap.LoadFromFile(LTexPath + 'harshbricks-height5-16.png');
end;

procedure TForm1.Timer1Timer(Sender: TObject);
begin
  GorillaSphere1.RotationAngle.Y := GorillaSphere1.RotationAngle.Y + 10;
end;

end.
