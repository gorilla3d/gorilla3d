﻿unit Unit1;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,
  Gorilla.Viewport, System.Math.Vectors, FMX.Types3D, Gorilla.Control,
  Gorilla.Mesh, Gorilla.Terrain, FMX.Controls3D, Gorilla.Light, Gorilla.Camera,
  FMX.Objects3D, FMX.MaterialSources, Gorilla.Material.Default,
  Gorilla.Material.Terrain, Gorilla.Plane, Gorilla.Material.Water, Gorilla.Cube,
  Gorilla.SkyBox, Gorilla.Controller.Passes.Refraction, Gorilla.Controller,
  Gorilla.Controller.Passes.Reflection;

type
  TForm2 = class(TForm)
    GorillaViewport1: TGorillaViewport;
    Dummy1: TDummy;
    GorillaCamera1: TGorillaCamera;
    GorillaLight1: TGorillaLight;
    GorillaTerrain1: TGorillaTerrain;
    GorillaTerrainMaterialSource1: TGorillaTerrainMaterialSource;
    GorillaPlane1: TGorillaPlane;
    Timer1: TTimer;
    procedure Timer1Timer(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    FReflection : TGorillaRenderPassReflection;
    FRefraction : TGorillaRenderPassRefraction;
    FWaterMaterial : TGorillaWaterMaterialSource;
  public
    { Public-Deklarationen }
  end;

var
  Form2: TForm2;

implementation

{$R *.fmx}

uses
  System.IOUtils;

procedure TForm2.FormCreate(Sender: TObject);
var LTexPath : String;
begin
{$IFDEF ANDROID}
  LTexPath := TPath.GetHomePath();
{$ENDIF}
{$IFDEF MSWINDOWS}
  LTexPath := IncludeTrailingPathDelimiter(
    IncludeTrailingPathDelimiter(ExtractFilePath(ParamStr(0))) + 'textures');
{$ENDIF}

  /// Create reflection render pass for water material
  FReflection := TGorillaRenderPassReflection.Create(GorillaViewport1);
  FReflection.Viewport := GorillaViewport1;
  FReflection.Camera := GorillaCamera1;
  FReflection.IgnoreControl(GorillaPlane1);
  FReflection.MirrorSize := GorillaPlane1.Width;

  // Set the current position of the water plane as mirror plane.
  // This needs to be updated, if water plane moves.
  FReflection.MirrorPosition := TPoint3D(GorillaPlane1.AbsolutePosition);
  FReflection.Enabled := true;

  /// create refraction render pass for water material
  FRefraction := TGorillaRenderPassRefraction.Create(GorillaViewport1);
  FRefraction.Viewport := GorillaViewport1;
  FRefraction.IgnoreControl(GorillaPlane1);
  FRefraction.Enabled := true;

  /// BUGFIX:
  /// If textures are loaded by IDE (designtime), it will create simple TBitmap instances.
  /// All Gorilla3D specific texture settings (like format, wrapping, ...) will be lost!
  /// This will lead to bad water rendering, so we better load textures at runtime.
  FWaterMaterial := TGorillaWaterMaterialSource.Create(GorillaPlane1);
  FWaterMaterial.Parent := GorillaPlane1;
  FWaterMaterial.NormalMap.LoadFromFile(LTexPath + 'water-normal.jpg');
  FWaterMaterial.DUDVTexture.LoadFromFile(LTexPath + 'water-dudv.jpg');
  FWaterMaterial.DisplacementMap.LoadFromFile(LTexPath + 'water-height.jpg');
  FWaterMaterial.SpecularMap.LoadFromFile(LTexPath + 'water-spec.jpg');
  FWaterMaterial.FoamTexture.LoadFromFile(LTexPath + 'water-foam.jpg');

  /// link reflection and refraction render pass to water material
  FWaterMaterial.ReflectionPass := FReflection;
  FWaterMaterial.RefractionPass := FRefraction;

  GorillaPlane1.MaterialSource := FWaterMaterial;
end;

procedure TForm2.Timer1Timer(Sender: TObject);
begin
  GorillaViewport1.Invalidate;
end;

end.
