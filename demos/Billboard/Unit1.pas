unit Unit1;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,
  System.Math.Vectors, FMX.Types3D, FMX.Controls3D, Gorilla.Light,
  Gorilla.Camera, FMX.Objects3D, Gorilla.Viewport, Gorilla.Control,
  Gorilla.Mesh, Gorilla.Billboard, FMX.MaterialSources,
  Gorilla.Material.Default, Gorilla.Material.Grass, Gorilla.Material.Types;

type
  TForm1 = class(TForm)
    GorillaViewport1: TGorillaViewport;
    Dummy1: TDummy;
    GorillaCamera1: TGorillaCamera;
    GorillaLight1: TGorillaLight;
    GorillaBillboard1: TGorillaBillboard;
    GorillaGrassMaterialSource1: TGorillaGrassMaterialSource;
    Plane1: TPlane;
    Timer1: TTimer;
    procedure FormCreate(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
  private
    { Private-Deklarationen }
  public
    { Public-Deklarationen }
  end;

var
  Form1: TForm1;

implementation

{$R *.fmx}

uses
  System.IOUtils,
  Gorilla.Utils.Math;

procedure TForm1.FormCreate(Sender: TObject);
var LPath  : String;
    LTexPath : String;
    LPoolE : TGorillaBitmapPoolEntry;
    LAlg   : TGorillaBillboardFlatFilling;
begin
  Randomize();

  LPath := '';
{$IFDEF ANDROID}
  LPath := IncludeTrailingPathDelimiter(TPath.GetHomePath());
{$ENDIF}

  LTexPath := LPath + IncludeTrailingPathDelimiter('textures');

  // load grass material
  with GorillaGrassMaterialSource1 do
  begin
    LPoolE := Bitmaps.Add() as TGorillaBitmapPoolEntry;
    LPoolE.DisplayName := 'Grass1';
    LPoolE.Bitmap.LoadFromFile(LTexPath + 'grass1.png');

    LPoolE := Bitmaps.Add() as TGorillaBitmapPoolEntry;
    LPoolE.DisplayName := 'Grass2';
    LPoolE.Bitmap.LoadFromFile(LTexPath + 'grass2.png');

    LPoolE := Bitmaps.Add() as TGorillaBitmapPoolEntry;
    LPoolE.DisplayName := 'Grass3';
    LPoolE.Bitmap.LoadFromFile(LTexPath + 'grass3.png');

    LPoolE := Bitmaps.Add() as TGorillaBitmapPoolEntry;
    LPoolE.DisplayName := 'Grass4';
    LPoolE.Bitmap.LoadFromFile(LTexPath + 'grass4.png');
  end;

  GorillaBillboard1.MaterialSource := GorillaGrassMaterialSource1;
  GorillaBillboard1.AddSourceObject(Plane1);

  LAlg := TGorillaBillboardFlatFilling.Create(GorillaBillboard1);
  try
    GorillaBillboard1.Fill(LAlg, true);
  finally
    FreeAndNil(LAlg);
  end;

  Timer1.Enabled := true;
end;

procedure TForm1.Timer1Timer(Sender: TObject);
begin
  GorillaViewport1.BeginUpdate();
  try
    // rotate camera
    Dummy1.RotationAngle.Y := Dummy1.RotationAngle.Y + 0.1;
  finally
    GorillaViewport1.EndUpdate();
  end;
end;

end.
