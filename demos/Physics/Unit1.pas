unit Unit1;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,
  Gorilla.Viewport, System.Math.Vectors, FMX.Types3D, FMX.Objects3D,
  FMX.Controls3D, Gorilla.Control, Gorilla.Mesh, Gorilla.Plane, Gorilla.Cube,
  Gorilla.Sphere, Gorilla.Material.Phong, Gorilla.Material.Default,
  Gorilla.Material.Lambert, FMX.MaterialSources, Gorilla.Physics;

type
  TForm1 = class(TForm)
    GorillaViewport1: TGorillaViewport;
    Camera1: TCamera;
    Light1: TLight;
    Dummy1: TDummy;
    GorillaPlane1: TGorillaPlane;
    GorillaSphere1: TGorillaSphere;
    GorillaCube1: TGorillaCube;
    LightMaterialSource1: TLightMaterialSource;
    GorillaLambertMaterialSource1: TGorillaLambertMaterialSource;
    GorillaPhongMaterialSource1: TGorillaPhongMaterialSource;
    GorillaPhysicsSystem1: TGorillaPhysicsSystem;
    Timer1: TTimer;
    procedure FormCreate(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
  private
    { Private-Deklarationen }
  public
    { Public-Deklarationen }
  end;

var
  Form1: TForm1;

implementation

{$R *.fmx}


uses
  System.IOUtils,
  Gorilla.Physics.Q3.Body;

procedure TForm1.FormCreate(Sender: TObject);
var LPath : String;
    LPrefab : TGorillaColliderSettings;
begin
  LPath := '';
{$IFDEF ANDROID}
  LPath := IncludeTrailingPathDelimiters(TPath.GetHomePath());
{$ENDIF}

  // load textures for plane, cube and sphere
  LightMaterialSource1.Texture.LoadFromFile(LPath + 'crate1_diffuse.png');
  GorillaLambertMaterialSource1.Texture.LoadFromFile(LPath + 'floor-wood.jpg');
  GorillaPhongMaterialSource1.Texture.LoadFromFile(LPath + 'candycane.jpg');

  // setup physics

  // create a static collider for the plane
  LPrefab := TGorillaColliderSettings.Create(TQ3BodyType.eStaticBody);
  GorillaPhysicsSystem1.AddBoxCollider(GorillaPlane1, LPrefab);

  // create a collider for the cube
  LPrefab := TGorillaColliderSettings.Create(TQ3BodyType.eDynamicBody);
  GorillaPhysicsSystem1.AddBoxCollider(GorillaCube1, LPrefab);

  // create a collider for the sphere
  LPrefab := TGorillaColliderSettings.Create(TQ3BodyType.eDynamicBody);
  GorillaPhysicsSystem1.AddSphereCollider(GorillaSphere1, LPrefab);

  // we activate the physics system
  GorillaPhysicsSystem1.Active := true;

  // and we need to activate the physics timer for constant updates
  Timer1.Enabled := true;
end;

procedure TForm1.Timer1Timer(Sender: TObject);
var LDelta : Single;
begin
  // get the delta time => time since the last step() was called
  LDelta := GorillaPhysicsSystem1.GetDeltaTime();
  GorillaPhysicsSystem1.Step(LDelta);
  GorillaViewport1.Invalidate();
end;

end.
