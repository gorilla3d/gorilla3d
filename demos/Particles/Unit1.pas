unit Unit1;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,
  System.Math.Vectors, FMX.Types3D, FMX.Controls3D, Gorilla.Light,
  Gorilla.Camera, FMX.Objects3D, Gorilla.Viewport, Gorilla.Particle.Influencer,
  Gorilla.Particle.Emitter, Gorilla.Particle.Smoke, Gorilla.Control,
  Gorilla.Mesh, Gorilla.Plane, FMX.MaterialSources, Gorilla.Material.Default,
  Gorilla.Material.Particle;

type
  TForm1 = class(TForm)
    GorillaViewport1: TGorillaViewport;
    Dummy1: TDummy;
    GorillaCamera1: TGorillaCamera;
    GorillaLight1: TGorillaLight;
    GorillaPlane1: TGorillaPlane;
    GorillaSmokeParticleEmitter1: TGorillaSmokeParticleEmitter;
    Timer1: TTimer;
    procedure FormCreate(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
  private
    { Private-Deklarationen }
  public
    { Public-Deklarationen }
  end;

var
  Form1: TForm1;

implementation

{$R *.fmx}

uses
  System.IOUtils;

procedure TForm1.FormCreate(Sender: TObject);
begin
{$IFDEF MSWINDOWS}
  GorillaSmokeParticleEmitter1.LoadTexture(
    'smoke.png');
{$ENDIF}
{$IFDEF ANDROID}
  GorillaSmokeParticleEmitter1.LoadTexture(
    IncludeTrailingPathDelimiter(TPath.GetHomePath()) + 'smoke.png');
{$ENDIF}

  // adjust particle size
  GorillaSmokeParticleEmitter1.ParticleSize   := TParticlePreset.Create(16, 16, 1, false);
  GorillaSmokeParticleEmitter1.ParticleGrowth := TParticlePreset.Create(4, 8, 1, false);

  GorillaSmokeParticleEmitter1.Start();
end;

procedure TForm1.Timer1Timer(Sender: TObject);
begin
  Dummy1.RotationAngle.Y := Dummy1.RotationAngle.Y + 5;
end;

end.
